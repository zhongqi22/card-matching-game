//
//  CardCollectionViewCell.swift
//  MatchCardGame
//
//  Created by zhongqi on 13/02/2019.
//  Copyright © 2019 zhongqi. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var frontImageView: UIImageView!
    
    @IBOutlet weak var backImageView: UIImageView!
    
    //optional Card because it is nil initially
    var card: Card?
    
    func setCard(_ card: Card) {
        //know the card get passed it
        self.card = card
        
        frontImageView.image = UIImage(named: card.imageName)
        
        if card.isMatched {
            backImageView.alpha = 0
            frontImageView.alpha = 0
            
            return
        } else {
            backImageView.alpha = 1
            frontImageView.alpha = 1
        }

        if card.isFlipped {
            UIView.transition(from: backImageView, to: frontImageView, duration: 0, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        } else {
            UIView.transition(from: frontImageView, to: backImageView, duration: 0, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        }
        

    }
    
    func flip() {
        UIView.transition(from: backImageView, to: frontImageView, duration: 0.3, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        
    
    }
    
    func flipBack() {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5){
            UIView.transition(from: self.frontImageView, to: self.backImageView, duration: 0.3, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        }
        
    }
    
    func remove(){
        backImageView.alpha = 0
        
        UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseOut, animations: {
            self.frontImageView.alpha = 0
        }, completion: nil)
    }
    
    
}
