//
//  CardModel.swift
//  MatchCardGame
//
//  Created by zhongqi on 13/02/2019.
//  Copyright © 2019 zhongqi. All rights reserved.
//

import Foundation

class CardModel {
    
    func getCards() -> [Card]{
    
        //1. Declare an array to store the generated cards
        var generatedCardsArray = [Card]()
        var randNumArray = [Int]()
        //2. Randomly generate pairs of card
        
        while randNumArray.count < 8 {
            let randNum = arc4random_uniform(13) + 1
            if randNumArray.count > 0 && randNumArray.contains(Int(randNum)){
                continue
            }
            
            randNumArray.append(Int(randNum))
            let cardOne = Card()
            cardOne.imageName = "card\(randNum)"
            
            generatedCardsArray.append(cardOne)
            
            let cardTwo = Card()
            cardTwo.imageName = "card\(randNum)"
            
            generatedCardsArray.append(cardTwo)
            
        }
        
        //randomize cards
//        for i in 0...generatedCardsArray.count-1 {
//            let randomNumber = Int(arc4random_uniform(UInt32(generatedCardsArray.count)))
//
//            let temporaryCard = generatedCardsArray[i]
//            generatedCardsArray[i] = generatedCardsArray[randomNumber]
//            generatedCardsArray[randomNumber] = temporaryCard
//        }
        
//        for _ in 1...8 {
//
//            let randNum = arc4random_uniform(13) + 1
//
//            print(randNum)
//
//            let cardOne = Card()
//            cardOne.imageName = "card\(randNum)"
//
//            generatedCardsArray.append(cardOne)
//
//            let cardTwo = Card()
//            cardTwo.imageName = "card\(randNum)"
//
//            generatedCardsArray.append(cardTwo)
//
//            //make unique pair of cards
//
//        }
        
        //3. TODO Randomize the array
        
        
        
        //4. Return the array
        print(randNumArray)
        return generatedCardsArray.shuffled()
    }
    
}
