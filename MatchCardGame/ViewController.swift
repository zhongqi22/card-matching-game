//
//  ViewController.swift
//  MatchCardGame
//
//  Created by zhongqi on 13/02/2019.
//  Copyright © 2019 zhongqi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var model = CardModel()
    var cardArray = [Card]()

    var soundManager = SoundManager()
    
    var firstFlippedCardIndex: IndexPath?
    
    var timer: Timer?

    var milliseconds: Float = 10 * 1000 //10seconds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        cardArray = model.getCards()
        // Do any additional setup after loading the view, typically from a nib.
        
        //create timer object
        timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(timerElapsed), userInfo: nil, repeats: true)
        
        RunLoop.main.add(timer!, forMode: .common)


    }
    
    override func viewDidAppear(_ animated: Bool) {
        soundManager.playSound(.shuffle)
    }
    
    //Timer methods
    @objc func timerElapsed(){
        milliseconds -= 1
        
        //convert to seconds in String, to limit to 2dp
        let seconds = String(format: "%.2f", milliseconds/1000)
        timeLabel.text = "Time Remaining: \(seconds)"
        
        //when timer is reaching 0, stop it
        if milliseconds <= 0 {
            timer?.invalidate()
            timeLabel.textColor = UIColor.red
            
            isGameEnded()
        }
    }
    
    //MARK: UICollectionView Protocol Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCollectionViewCell
    
        let card = cardArray[indexPath.row]
        cell.setCard(card)
        
        return cell
    }
    
    //MARK: Game Logic Section
    
    func collectionView (_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //check is there any time left
        if milliseconds <= 0 {
            return
        }
        
        
        //TODO logic here when the card is tapped
        let cell = collectionView.cellForItem(at: indexPath) as! CardCollectionViewCell
        
        let card = cardArray[indexPath.row]
        
        if !card.isFlipped && !card.isMatched {
            
            cell.flip()
            
            soundManager.playSound(.flip)
            
            card.isFlipped = true
            
            if firstFlippedCardIndex == nil {
                firstFlippedCardIndex = indexPath
            } else {
                checkCardMatching(indexPath)
            }
        }
    }
    
    
    
    func checkCardMatching(_ secondFlippedCardIndex: IndexPath){
        let cardOneCell = collectionView.cellForItem(at: firstFlippedCardIndex!) as? CardCollectionViewCell
        let cardTwoCell = collectionView.cellForItem(at: secondFlippedCardIndex) as? CardCollectionViewCell
        
        let cardOne = cardArray[firstFlippedCardIndex!.row]
        let cardTwo = cardArray[secondFlippedCardIndex.row]
        
        if cardOne.imageName == cardTwo.imageName {
            
            soundManager.playSound(.match)
            
            cardOne.isMatched = true
            cardTwo.isMatched = true
            
            cardOneCell?.remove()
            cardTwoCell?.remove()
            
            isGameEnded()
            
        } else {
            
            soundManager.playSound(.nomatch)
            
            cardOne.isFlipped = false
            cardTwo.isFlipped = false
            
            cardOneCell?.flipBack()
            cardTwoCell?.flipBack()
            
            
        }
        
        if cardOneCell == nil {
            collectionView.reloadItems(at: [firstFlippedCardIndex!])
        }
        
        firstFlippedCardIndex = nil
        
        
    }
    
    func isGameEnded(){
        //check is all the card isMatched
        var isWon = true
        
        for card in cardArray {
            if !card.isMatched {
                isWon = false
                break;
            }
        }
        
        var title = ""
        var msg = ""
        
        
        if isWon {
            
            if milliseconds > 0 {
                timer?.invalidate()
            }
            
            title = "Congratulation!"
            msg = "You've won!"
        } else {
            
            if milliseconds > 0 {
                return
            }
            
            title = "Game Over"
            msg = "You've lost :("
            
        }
        
        showAlert(title, msg)
        
    }
    
    func showAlert(_ title: String, _ msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(alertAction)
        
        present(alert, animated: true, completion: nil)
    }


}

