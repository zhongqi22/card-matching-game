//
//  Card.swift
//  MatchCardGame
//
//  Created by zhongqi on 13/02/2019.
//  Copyright © 2019 zhongqi. All rights reserved.
//

import Foundation

class Card {
    
    var imageName = ""
    var isFlipped = false
    var isMatched = false
    
}
