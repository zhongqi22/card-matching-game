//
//  SoundManager.swift
//  MatchCardGame
//
//  Created by zhongqi on 14/02/2019.
//  Copyright © 2019 zhongqi. All rights reserved.
//

import Foundation
import AVFoundation

class SoundManager {
    
    var audioPlayer: AVAudioPlayer?
    
    enum SoundEffect {
        
        case flip
        case shuffle
        case match
        case nomatch
        
    }
    
    
    func playSound(_ effect: SoundEffect){
        
        var soundFileName = ""
        
        switch effect {
            case .flip:
                soundFileName = "cardflip"
            
            case .shuffle:
                soundFileName = "shuffle"
            
            case .match:
                soundFileName = "dingcorrect"
            
            case .nomatch:
                soundFileName = "dingwrong"
        }
        
        let bundlePath = Bundle.main.path(forResource: soundFileName, ofType: "wav")
    
        guard bundlePath != nil else {
            print("Couldn't find sound file named \(soundFileName).wav in the bundle")
            return
        }
        
        let soundURL = URL(fileURLWithPath: bundlePath!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            
            audioPlayer?.play() 
        } catch {
            print("Couldn't create sound file \(soundFileName)")
        }

    }

}
